#pragma once
#include "protocol.h"
#include <stdio.h>
#include <list>
#include <string>

using namespace std;

list<package>& get_file_packages(string fpath);

void to_file(list<package> pkgs, string fpath);
