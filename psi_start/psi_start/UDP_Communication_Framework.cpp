// UDP_Communication_Framework.cpp : Defines the entry point for the console application.
//

#pragma comment(lib, "ws2_32.lib")
#include "stdafx.h"
#include <winsock2.h>
#include "ws2tcpip.h"
#include <stdio.h>

#include <iostream>
#include <sstream>
#include "protocol.h"
#include "filefrag.h"
#include "communicator.h"
#include "pkghash.h"


void send() {
	sender sender(8888, 5555);

	if (!sender.socket_inited)
		return;

	string fname = "cernets.jpg";
	list<package> pkgs = get_file_packages(fname);

	string hash = get_pkgs_hash(pkgs);
	string hash1 = get_pkgs_hash(pkgs);

	string head_cont = fname + '|' + hash;
	
	package head = {0, pkgs.size() + 1};
	for (int i = 0; i < head_cont.length(); i++) {
		head.bytes[i] = head_cont.data()[i];
	}
	
	pkgs.push_front(head);
	sender.send(pkgs, 1000);
}

void receive() {
	receiver rec(8888, 5555);

	if (!rec.socket_inited)
		return;

	list<package> pkgs;
	string fname;
	bool file_received = true;
	do {
		pkgs = rec.receive(100000);
		if (pkgs.size() == 0) {
			perror("Failed to receive packages!");
			return;
		}

		package head = pkgs.front();
		pkgs.pop_front();

		string head_cont = head.bytes;
		istringstream cont_s(head_cont);

		getline(cont_s, fname, '|');
		string hash_expected;
		getline(cont_s, hash_expected, '\0');

		string hash = get_pkgs_hash(pkgs);
		if (hash != hash_expected) {
			printf("Hash failed\n");
			rec.send_file_assertion(false);
			printf("False file assertion sent\n");
			file_received = false;
		}
	} while (!file_received);

	rec.send_file_assertion(true);

	to_file(pkgs, fname);
}


int main()
{
	/*while (1) {
		receive();
	}*/
	send();

	getchar();
	return 0;
}
