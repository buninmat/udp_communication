#pragma once
#include "stdafx.h"
#include <winsock2.h>
#include "protocol.h"
#include "ws2tcpip.h"
#include <list>
#include <string>

#include "Crc32.h"

#define TARGET_IP	"147.32.216.152"

using namespace std;

enum AckType { FALSE_ACK, TRUE_PKG_ACK, TRUE_FILE_ACK, NO_ACK };

class communicator {

public:
	int target_port;
	int local_port;

	bool socket_inited;

	communicator(int tp, int lp) :target_port(tp), local_port(lp) {
		socket_inited = init_socket();
	}

protected:
	SOCKET socketS;
	struct sockaddr_in from;
	sockaddr_in addrDest;

	bool init_socket();
	AckType receive_ack(int idx);
	char *receive_pkg_bytes();
	void set_waiting_time(DWORD msec);

	~communicator() {
		closesocket(socketS);
	}
};

class receiver : public communicator {
public:
	receiver(int tp, int lp) :communicator(tp, lp) {}
	list<package> &receive(int waiting_ms);
	void send_file_assertion(bool ok);
};

class sender : public communicator {
public:
	sender(int tp, int lp) :communicator(tp, lp) {}
	void send(list<package> pkg, int waiting_ms);
};