#pragma once

#include <stdint.h>

#define PACKAGE_LEN 1024

#define DATA_LEN 1012

#define STOP_BYTE 13			//fills STOP package
#define PKG_ERROR_BYTE 33		//fills false package acknowledgement
#define PKG_OK_BYTE 42			//fills true package acknowledgement
#define SEQ_ERROR_BYTE 66		//fills false sequence acknowledgement
#define SEQ_OK_BYTE 77			//fills true sequence acknowledgement

typedef struct package {
	/* 
	 * packages may come reordered, hense idx
	 * idx = 0 - HEAD package, contains file name and number of packages
	 * datalen = length of bytes sub-array containing data OR for HEAD package - number of packages
	 */
	int idx;
	int datalen;
	char bytes[DATA_LEN];
};
