#include "pkghash.h"

using namespace std;

string get_pkgs_hash(list<package> pkgs) {
	md5wrapper md5;
	for (package pkg : pkgs) {
		unsigned char *pkg_bytes = new unsigned char[pkg.datalen];
		memcpy(pkg_bytes, &pkg.bytes, pkg.datalen);
		md5.updateContext(pkg_bytes, pkg.datalen);
	}
	return md5.hashIt();
}