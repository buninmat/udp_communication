#pragma once
#include "communicator.h"


void InitWinsock()
{
	WSADATA wsaData;
	WSAStartup(MAKEWORD(2, 2), &wsaData);
}

bool communicator::init_socket() {

	InitWinsock();

	/*****socket*****/
	struct sockaddr_in local;

	local.sin_family = AF_INET;
	local.sin_port = htons(local_port);
	local.sin_addr.s_addr = INADDR_ANY;

	socketS = socket(AF_INET, SOCK_DGRAM, 0);
	if (bind(socketS, (sockaddr*)&local, sizeof(local)) != 0) {
		printf("Binding error!\n");
		return false;
	}

	/********destination*********/
	addrDest.sin_family = AF_INET;
	addrDest.sin_port = htons(target_port);
	InetPton(AF_INET, _T(TARGET_IP), &addrDest.sin_addr.s_addr);

	return true;
}

void communicator::set_waiting_time(DWORD msec) {
	DWORD val = msec;
	setsockopt(socketS, SOL_SOCKET, SO_RCVTIMEO, (const char*)&val, sizeof DWORD);
	/*fd_set fds;
	int n;
	struct timeval tv;

	// Set up the file descriptor set.
	FD_ZERO(&fds);
	FD_SET(socketS, &fds);

	// Set up the struct timeval for the timeout.
	tv.tv_sec = 10;
	tv.tv_usec = 0;

	// Wait until timeout or data received.
	n = select(socketS, &fds, NULL, NULL, &tv);
	if (n == 0)
	{
		printf("Timeout..\n");
	}
	else if (n == -1)
	{
		printf("Error while setting timeout\n");
	}*/
}

#define MAX_ERROR_N 5

static bool is_of_bytes(char *bytes, char byte) {
	for (int i = 0; i < PACKAGE_LEN; i++)
		if (bytes[i] != byte)
			return false;
	return true;
}

static char *get_bytes(char byte) {
	char *bytes = new char[PACKAGE_LEN];
	for (int i = 0; i < PACKAGE_LEN; i++)
		bytes[i] = byte;
	return bytes;
}

static bool is_ack_of_bytes(char byte, package pkg) {
	int error_c = 0;
	for (int i = 0; i < pkg.datalen; i++)
		if (pkg.bytes[i] != byte)
		{
			error_c++;
			if (error_c == MAX_ERROR_N)
				return false;
		}
	return true;
}

static char *get_ack_bytes(int idx, char byte) {
	char *bytes = new char[PACKAGE_LEN];
	package pkg{ idx, DATA_LEN };

	for (int i = 0; i < DATA_LEN; i++)
		pkg.bytes[i] = byte;

	memcpy(bytes, &pkg, PACKAGE_LEN);

	return bytes;
}

AckType communicator::receive_ack(int idx) {

	char *ack_bytes = receive_pkg_bytes();
	if (ack_bytes == nullptr)
		return NO_ACK;
	package pkg;
	memcpy(&pkg, ack_bytes, PACKAGE_LEN);
	if (is_ack_of_bytes(PKG_OK_BYTE, pkg)) {
		printf("True acknowledgement\n");
		return TRUE_PKG_ACK;
	}
	if (is_ack_of_bytes(PKG_ERROR_BYTE, pkg)) {
		printf("False acknowledgement\n");
		return FALSE_ACK;
	}
	if (is_ack_of_bytes(SEQ_ERROR_BYTE, pkg)) {
		printf("False file assertion\n");
		return FALSE_ACK;
	}
	if (is_ack_of_bytes(SEQ_OK_BYTE, pkg)) {
		printf("True file assertion\n");
		return TRUE_FILE_ACK;
	}
	printf("Error while receiving acknowledgement\n");
	return NO_ACK;
}

char * communicator::receive_pkg_bytes() {
	char *pkg_buf = get_ack_bytes(0, 0);

	int fromlen = sizeof(from);

	int res = recvfrom(socketS, pkg_buf, PACKAGE_LEN, 0, (sockaddr*)&from, &fromlen);

	if (res == SOCKET_ERROR) {
		printf("Socket error!(Timed out)\n");
		return nullptr;
	}

	if (is_of_bytes(pkg_buf, 0)) {
		printf("Nothing received!");
		return nullptr;
	}

	if (is_of_bytes(pkg_buf, STOP_BYTE))
		return nullptr;

	return pkg_buf;
}

void sender::send(list<package> pkgs, int waiting_ms) {

	set_waiting_time(waiting_ms);

	bool file_asserted;
	do {
		for (package pkg : pkgs) {
			char pkg_bytes[PACKAGE_LEN];
			uint32_t crc;
			memcpy(pkg_bytes, &pkg, PACKAGE_LEN - sizeof(crc));
			crc = crc32_fast(pkg_bytes, PACKAGE_LEN - sizeof(crc));
			memcpy(pkg_bytes + (PACKAGE_LEN - sizeof(crc)), &crc, sizeof(crc));

			bool received;
			int cnt = 0;
			do {
				sendto(socketS, pkg_bytes, PACKAGE_LEN, 0, (sockaddr*)&addrDest, sizeof(addrDest));
				printf("Sending package %d\n", pkg.idx);
				received = receive_ack(pkg.idx) == TRUE_PKG_ACK;
				cnt++;
				if (cnt == 100) {
					perror("Failed to send packages\n");
					return;
				}
			} while (!received);

		}

		AckType finalAck;
		do {
			sendto(socketS, get_bytes(STOP_BYTE), PACKAGE_LEN, 0, (sockaddr*)&addrDest, sizeof(addrDest));
			finalAck = receive_ack(-1);
		} while (finalAck == NO_ACK);

		file_asserted = finalAck == TRUE_FILE_ACK;

	} while (!file_asserted);

	printf("Packages are sent\n");
}

static void link_package(list<package> *pkgs, package &pkg) {
	if (pkgs->size() == 0) {
		pkgs->push_back(pkg);
		return;
	}
	auto i = pkgs->cend();
	do {
		i--;
		if (pkg.idx == i->idx) {
			i = pkgs->erase(i);
			pkgs->emplace(i, pkg);
			return;
		}
		if (i->idx < pkg.idx) {
			pkgs->emplace(next(i), pkg);
			return;
		}
	} while (i != pkgs->cbegin());
	pkgs->emplace(pkgs->begin(), pkg);
}

list<package> &receiver::receive(int waiting_ms) {

	uint32_t crc_rec;
	uint32_t crc_comp;

	set_waiting_time(waiting_ms);

	printf("Waiting for datagram ...\n");

	list<package> *pkgs = new list<package>();
	while (1) {
		/************Fetching package*************/
		char* pkg_buf = receive_pkg_bytes();
		if (pkg_buf == nullptr) {
			printf("Waiting for datagram terminated\n");
			break;
		}
		/**************Unpack package****************/
		package pkg;
		memcpy(&pkg, pkg_buf, PACKAGE_LEN - sizeof(crc_rec));
		/***********checking CRC*************/
		memcpy(&crc_rec, pkg_buf + (PACKAGE_LEN - sizeof(crc_rec)), sizeof(crc_rec));
		crc_comp = crc32_fast(pkg_buf, PACKAGE_LEN - sizeof(crc_comp));
		/**********creating aknowledgement************/
		char *ack_bytes;
		if (crc_comp != crc_rec) {
			ack_bytes = get_ack_bytes(pkg.idx, PKG_ERROR_BYTE);
			printf("Error in received package\n");
		}
		else {
			ack_bytes = get_ack_bytes(pkg.idx, PKG_OK_BYTE);
			
			printf("Package number %d fetched!\n", pkg.idx);

			link_package(pkgs, pkg);
		}
		/**********sending acknowledgement*************/
		sendto(socketS, ack_bytes, PACKAGE_LEN, 0, (sockaddr*)&addrDest, sizeof(addrDest));
		printf("Response send\n");
	}
	int pkg_cnt = pkgs->empty() ? 0 : pkgs->front().datalen - pkgs->size();
	printf("Receiving finished: %d lost\n", pkg_cnt);

	return *pkgs;
}

void receiver::send_file_assertion(bool ok) {
	char *assert_bytes = get_ack_bytes(-1, ok ? SEQ_OK_BYTE : SEQ_ERROR_BYTE);
	sendto(socketS, assert_bytes, PACKAGE_LEN, 0, (sockaddr*)&addrDest, sizeof(addrDest));
}
