#include "filefrag.h"

list<package> &fragment(char *buf, int len);

list<package> &get_file_packages(string fpath) {

	FILE *fo = fopen(fpath.data(), "rb");
	if (fo == nullptr) {
		printf("File not found!\n");
		return *new list<package>();;
	}

	fseek(fo, 0, SEEK_END);
	int len = ftell(fo);
	rewind(fo);

	char *buf = new char[len];

	int read = fread(buf, len, 1, fo);
	if (!read) {
		printf("Error while reading file!");
		return *new list<package>();
	}

	fclose(fo);

	return fragment(buf, len);
}

list<package> &fragment(char *buf, int len) {
	list<package> *pkgs = new list<package>();
	int pkg_cnt = len / DATA_LEN + 1;
	for (int i = 0; i != pkg_cnt; i++) {

		package *pkg = new package();
		pkg->datalen = i == pkg_cnt - 1 ? len % DATA_LEN : DATA_LEN;
		pkg->idx = i + 1;

		int start = i * DATA_LEN;
		for (int j = 0; j < pkg->datalen; j++) {
			pkg->bytes[j] = buf[start + j];
		}

		pkgs->push_back(*pkg);
	}
	return *pkgs;
}

void to_file(list<package> pkgs, string fpath) {
	if (pkgs.empty()) {
		printf("Nothing to save!\n");
		return;
	}
	FILE *fo = fopen(fpath.data(), "wb");
	int buflen = (pkgs.size() - 1) * DATA_LEN + pkgs.back().datalen;
	char *buf = new char[buflen];
	int cnt = 0;
	for (auto pkg : pkgs) {
		for (int i = 0; i < pkg.datalen; i++) {

			buf[cnt * DATA_LEN + i] = pkg.bytes[i];
		}
		cnt++;
	}
	fwrite(buf, buflen, 1, fo);
	printf("File saved!\n");
	fclose(fo);
}
